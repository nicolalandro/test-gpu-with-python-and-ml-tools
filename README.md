# Test GPU and Python Machine Learning Tool
With this project it is possible to test if your ML environment work correctly.

## Tensorflow
* CPU
```python
python src/cpu_tensorflow.py
```
* GPU
```python
python src/gpu_tensorflow.py
```

## Pythorch
* CPU
```python
# not available
```
* GPU
```python
# not available
```
